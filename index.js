console.log('Hello World');

/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
		
*/
		function sumOf(num1, num2){
			let sum = num1 + num2;
			console.log('The sum of ' + num1 + ' and ' + num2 );
			console.log(+sum);
			

		};

		sumOf(5, 15);
		

		
		function differenceOf(num3, num4){
			let difference = num3 - num4;
			console.log('The difference of ' + num3 + ' and ' + num4 );
			console.log(+difference);
			

		};

		differenceOf(20, 5);
	
		
/*
	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.


*/
		function productOf(num){
			let product = num * 10;
			console.log('The product of ' + num + ' and 10:' );
			console.log(+product);
			

		};

		productOf(50);


		function quotientOf(num5){
			let quotient = num5 / 10;
			console.log('The quotient of ' + num5 + ' and 10:' );
			console.log(+quotient);
			

		};

		quotientOf(50);


/*

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

*/
		function circleArea(num7){
			
			let num6 = Math.pow(num7,2);
			console.log(num6); 
			let area = num6  * 3.1416;

			console.log('The result of getting the area of a circle with  ' + num7 +   ' radius' );
			console.log(+area);
			

		};

		circleArea(15);



/*
	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.

*/


		array = [20, 40, 60, 80];

		function averageVar(array) {
	    		let total = 0;
	    		let count = 0;

    			array.forEach(function(item, index) {
        		total += item;
        			count++;
    			});
    			console.log('The average of 20, 40, 60 and 80 is:' );
    			return total / count;
			}

			console.log(averageVar(array));





/*




	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

		function isPassingScore(num){
			let scorePer = num / 50;
			let isPassingScore =  scorePer = 37.5;
			console.log('Is ' + num + '/50 a passing score?');
			

			let isPassingScore1 = true;
			let isNotPassingScore1 = false;


			let allRequirementsMet1 = isPassingScore1 = !isNotPassingScore1;
			console.log(' ' + allRequirementsMet1);
		};

		isPassingScore(38);
		
		